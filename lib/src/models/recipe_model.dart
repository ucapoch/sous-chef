import 'package:firebase_database/firebase_database.dart';
import 'dart:convert';


class Recipe {
  String key;
  String origine;
  String titre;
  var ingredients;
  var recette;
  String imageURL;
  String source;

  Recipe({this.origine, this.titre, this.ingredients, this.recette, this.source, this.imageURL});

  Recipe.fromSnapshot(DataSnapshot snapshot) {
    key = snapshot.key;
    origine = snapshot.value["origine"];
    titre = snapshot.value["titre"];
    ingredients = snapshot.value["ingredients"];
    recette = snapshot.value["recette"];
    source = snapshot.value["source"];
    imageURL = snapshot.value["imageURL"];
  }

  toJson() {
    return {
      "key": key,
      "origine": origine,
      "titre": titre,
      "ingredients": jsonEncode(this.ingredients),
      "recette": jsonEncode(this.recette),
      "source": source,
      "imageURL": imageURL,
    };
  }

}