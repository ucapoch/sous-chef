import 'package:firebase_database/firebase_database.dart';
import 'dart:convert';

class User {
  String key;
  String email;
  String userName;
  String firstName;
  String lastName;
  String gender;
  int age;
  List<String> favoriteRecipes;

  User({this.email, this.userName, this.firstName, this.lastName, this.gender, this.age, this.favoriteRecipes});

  User.fromSnapshot(DataSnapshot snapshot) {
    key = snapshot.key;
    email = snapshot.value["email"];
    userName = snapshot.value["userName"];
    firstName = snapshot.value["firstName"];
    lastName = snapshot.value["lastName"];
    gender = snapshot.value["gender"];
    age = snapshot.value["age"];
    favoriteRecipes = snapshot.value["favoriteRecipes"];
  }

  toJson() {

    return {
      "key": key,
      "email": email,
      "userName": userName,
      "firstName": firstName,
      "lastName": lastName,
      "gender": gender,
      "age": age,
      "favoriteRecipes": jsonEncode(this.favoriteRecipes),
    };
  }

  void addRecipeToFavorites(String key) {
    favoriteRecipes.add(key);
  }

  void deleteRecipeFromFavorites(String key) {
    favoriteRecipes.remove(key);
  }
}