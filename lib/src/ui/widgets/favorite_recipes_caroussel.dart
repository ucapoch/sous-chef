import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../models/recipe_model.dart';
import '../screens/description.dart';
// import 'dart:convert';

class FavoriteRecipesCarousel extends StatelessWidget {
  final List<Recipe> recipes;

  FavoriteRecipesCarousel(this.recipes);

  PageController ctrl = PageController(viewportFraction: 0.8);
  
  @override
  Widget build(BuildContext context) {
    
    if(recipes.length > 0) {
      return Container(
        width: 375, 
        height: 500,
        color: Colors.transparent,
        child: ListView.builder(
          padding: EdgeInsets.only(top: 10, bottom: 10, left: 20),
          controller: ctrl,
          itemCount: recipes.length, 
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index){
            Recipe recipe = recipes[index];
            return GestureDetector( 
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => Description(recipe)));
              },
              child: Container(
              margin: EdgeInsets.only(right: 10), 
              width: 200, 
              decoration: BoxDecoration(
                boxShadow: [BoxShadow(
                  color: Color.fromRGBO(110, 102, 102, 0.5),
                  offset: Offset(0, 0),
                  blurRadius: 5)
                ],
                color: Colors.yellow,
                borderRadius: BorderRadius.circular(10),
                image: DecorationImage(image: NetworkImage(recipe.imageURL), fit: BoxFit.cover),
              ),
              child: Stack(children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      width:200,
                      height: 100,
                      decoration: BoxDecoration(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10),bottomRight:Radius.circular(10) ),
                      color: Color.fromRGBO(0, 0, 0, 0.25)), 
                      child: Padding(
                        padding: const EdgeInsets.only(top: 10, left: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(recipe.titre,style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold,color: Colors.white),),
                            SizedBox(height: 10),
                            Row(
                              children: <Widget>[
                                Icon(Icons.location_on,color: Colors.white, size: 18,),
                                SizedBox(width: 5),
                                Text(recipe.origine,style: TextStyle(fontSize: 18,color: Colors.white),),
                              ],
                            ),
                            // Row(children: <Widget>[
                            //   Icon(Icons.person,color: Colors.white,size: 30,),
                            //   // Text(recipe.nombre,style: TextStyle(fontSize: 20,color: Colors.white),),
                            // ],),
                          ],
                        ),
                      ),
                    ),
                  ],
                )
          ],),),);
        },),
      );
    }
    else {
      return Center(
        child: Text("Welcome. Your list is empty",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 20.0),
        )
      );
    }
  }
}