import 'package:flutter/material.dart';
import '../../models/recipe_model.dart';
import 'package:souschef/src/ui/screens/foundedRecipes.dart';

class FilterScreen extends StatefulWidget {
  final List<Recipe> recipes;

  FilterScreen(this.recipes);

  @override
  _FilterScreen createState() => _FilterScreen();
}

class _FilterScreen extends State<FilterScreen> {
  Map<String, bool> values = {
    'garlic': false,
    'yogurt': false,
    'olive oil': false,
    'lemon': false,
    'tomato': false,
    'paprika': false,
    'cumin': false,
    'oregano': false,
    'ground ginger': false,
    'nutmeg': false,
    'lamb shoulder': false,
    'Salt': false,
    'eggs': false,
    'coffee': false,
    'water': false,
  };

  List ings = [];

  @override
  Widget build(BuildContext context) {
    double dw = MediaQuery.of(context).size.width;
    double dh = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text('Filter'),
      ),
      body: Column(
        children: <Widget> [
          Container(
            height: dh - (dh / 3.5),
            child: ListView(
              padding: EdgeInsets.symmetric(horizontal: 5),
              scrollDirection: Axis.vertical,
              children: values.keys.map((String key) {
                return new CheckboxListTile(
                  title: new Text(key),
                  value: values[key],
                  onChanged: (bool value) {
                    setState(() {
                      values[key] = value;
                    });
                  },
                );
              }).toList(),
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 25, horizontal: 20),  
              child: ButtonTheme(
                minWidth: dw - 40,
                child: FlatButton(
                  onPressed: () {
                    setState(() {
                      // print(ings);
                      Navigator.push(context, MaterialPageRoute(builder: (context) => FoundedRecipes(searchRecipe(values, widget.recipes))));
                      // Navigator.push(context, MaterialPageRoute(builder: (context) => FoundedRecipes.fromFilter(searchRecipe(values, widget.recipes), ings)));
                      
                    });
                  }, 
                  child: Text('Submit', style: TextStyle(color: Colors.white, fontSize: 20),),
                  color: Colors.green,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
searchRecipe(Map<String, bool> m, List<Recipe> r) {
  List<Recipe> result;
  result = [];
  ings = [];
  m.forEach((k, v) {
    if(v){
      ings.add(k);
    }
  });
  // print(ings);
  if(ings.isEmpty) {
    result = [];
    return result;
  }
  else {
    r.forEach((r) {
      r.ingredients.forEach((ri) {
        ings.forEach((i) {
          if(ri != null && ri.toLowerCase().contains(i.toLowerCase())){
            if(result.contains(r)){}
            else{
              result.add(r);
            }
          }
        });
      });
    });
    return result;
  }
  
}

}


 