import 'package:flutter/material.dart';
import '../../models/recipe_model.dart';
import 'description.dart';

class FoundedRecipes extends StatefulWidget{
  List<Recipe> recipes;
  List ings;

  FoundedRecipes(this.recipes);
  FoundedRecipes.fromFilter(this.recipes, this.ings);

  @override
  _FoundedRecipes createState() => _FoundedRecipes();

}

class _FoundedRecipes extends State<FoundedRecipes> {

@override 
  Widget build(BuildContext context) {
    // print(widget.ings);
    double dw = MediaQuery.of(context).size.width;
    double dh = MediaQuery.of(context).size.height;
    
    return Scaffold(
      appBar: AppBar(
        title: Text('Recipes founded'),
        automaticallyImplyLeading: false,
        leading: IconButton(
          padding: EdgeInsets.only(left: 20),
          icon: Icon(Icons.arrow_back),
          color: Colors.white,
          iconSize: 25,
          onPressed: () {
            // print(widget.recipes.length);
            setState(() {
              widget.recipes = [];
              // print(widget.recipes.length);
            });
            Navigator.pop(context);
          },
        ),
      ),
      body: widget.recipes.isNotEmpty ? ListView.builder(
        // padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: widget.recipes.length,
        itemBuilder: (BuildContext context, int index) {
          Recipe recipe = widget.recipes[index];
          return GestureDetector(
            onTap: () {
              // Navigator.push(context, MaterialPageRoute(builder: (context) => Description.fromFilter(recipe, widget.ings)));
              Navigator.push(context, MaterialPageRoute(builder: (context) => Description(recipe)));
            },
            child: Stack(
              children: <Widget> [
                Container(
                  margin: EdgeInsets.fromLTRB(40.0, 5.0, 20.0, 5.0),
                  height: 190.0,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20.0),
                    boxShadow: [BoxShadow(
                      color: Color.fromRGBO(210, 202, 202, 0.5),
                      offset: Offset(0, 0),
                      blurRadius: 5)
                    ],
                  ),
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(100, 20, 20, 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget> [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              width: 110.0,
                              // padding: EdgeInsets.only(left: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(recipe.titre, style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20),maxLines: 2, overflow: TextOverflow.ellipsis,),
                                  SizedBox(height: 20),
                                  Text(recipe.origine, style: TextStyle(fontSize: 16),),
                                ],
                              ),
                            ),
                            IconButton(
                              iconSize: 25,
                              color: Colors.yellow,
                              icon: Icon(Icons.star_border),
                              onPressed: () {
                                return null;
                              },
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  left: 20.0,
                  top: 15.0,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20.0),
                    child: Image(
                      width: 110, 
                      image: NetworkImage(recipe.imageURL)
                    ),
                  ),
                )
              ], 
            ),
          );
        },
      ) : Center(child: Text("No recipe found",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 20.0),
        ))
    );
  }
}



  