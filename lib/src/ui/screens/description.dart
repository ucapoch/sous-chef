import 'package:flutter/material.dart';
import '../../models/recipe_model.dart';

class Description extends StatelessWidget{
  final Recipe recipe;
  List ingreds;

  Description(this.recipe);
  Description.fromFilter(this.recipe, this.ingreds);

  // List ings = [];
  // List ingsFromFilter = [];

  List<Widget> ingT = [];
  List<Widget> ingTFromFilter = [];

  List recs = [];
  List<Text> recT = [];

  List<Widget> showRec(List rec) {
    rec.forEach((r) {
      if(r != null) {
        recT.add(
          Text(r, style: TextStyle(fontStyle: FontStyle.italic),)
        );
      }
    });
    return recT;
  }

  List<Widget> showIng(List ing) {
    ing.forEach((i) {
      if(i != null) {
        ingT.add(Text(i, style: TextStyle(fontStyle: FontStyle.italic),));
      }
    });
    return ingT;
  }

  // List<Widget> showIngFromFilter(List ing) {
  //   ing.forEach((i) {
  //     ingreds.forEach((ingred) {
  //       if(i != null) {
  //         if(i.toLowerCase().contains(ingred.toLowerCase())) {
  //           ingTFromFilter.add(
  //             Row(
  //               mainAxisAlignment: MainAxisAlignment.start,
  //               children: <Widget>[
  //                 Icon(Icons.check, color: Colors.green,),
  //                 SizedBox(width: 5.0),
  //                 Text(i, style: TextStyle(fontStyle: FontStyle.italic),),
  //               ],
  //             )
  //           );
  //         }
  //         else 
  //         {
  //           ingTFromFilter.add(
  //             Row(
  //               mainAxisAlignment: MainAxisAlignment.start,
  //               children: <Widget>[
  //                 Icon(Icons.clear, color: Colors.red,),
  //                 SizedBox(width: 5.0),
  //                 Text(i, style: TextStyle(fontStyle: FontStyle.italic),),
  //               ],
  //             )
  //           );
  //         }
  //       }
  //     });
  //   });
  //   return ingT;
  // }
  
  @override 
  
  Widget build(BuildContext context) {
    double dw = MediaQuery.of(context).size.width;
    double dh = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                height: dw,
                width: dw,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black26,
                      offset: Offset(0.0, 0.2),
                      blurRadius: 6.0,
                    ),
                  ],   
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(30.0),
                  child: Image(
                    image: NetworkImage(recipe.imageURL),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 40.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    IconButton(
                      iconSize: 25,
                      color: Colors.white,
                      icon: Icon(Icons.chevron_left),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
          Expanded(
            child: ListView(
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 25.0),
                child: Text('Ingredients', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w400, decoration: TextDecoration.underline)),
              ),
              SizedBox(height: 8.0),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 25),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: showIng(recipe.ingredients).toList(),
                  // children: ingreds == null ? showIng(recipe.ingredients).toList() : showIngFromFilter(recipe.ingredients).toList(),
                ),
              ),
              SizedBox(height: 20.0),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 25.0),
                child: Text('Recipe', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w400, decoration: TextDecoration.underline)),
              ),
              SizedBox(height: 8.0),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 25),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: showRec(recipe.recette).toList(),
                ),
              ),
            ],
          ),
          ),
        ],
      ),
    );
  }
}