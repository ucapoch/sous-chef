import 'package:flutter/material.dart';

import 'src/services/authentication.dart';
import 'src/ui/screens/root_page.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        title: 'Flutter Login Demo',
        theme: new ThemeData(
          primarySwatch: Colors.green,
        ),
        home: new RootPage(auth: new Auth()),
        debugShowCheckedModeBanner: false,
    );
  }
}