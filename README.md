# souschef

A new Flutter application based on firebase.

## installation

- [Flutter: Follow this link to install flutter](https://flutter.dev/docs/get-started/install)


- [Get the project: Clone the project on gitlab](https://gitlab.com/ucapoch/sous-chef/)


## Install dependencies 
- open terminal in project directory
- run command: flutter pub get

## Run project
- excute command: flutter run


